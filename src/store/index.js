import Vue from 'vue'
import Vuex from 'vuex'
import modulesIndex from '../store/modules/index'
Vue.use(Vuex)


export default new Vuex.Store({
  modules: {
    modulesIndex
  },
  strict: true
})
