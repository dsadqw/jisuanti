//状态

const state = {
  isTemplate : '',
  set_number_info : ''
};

// 计算属性
const getters = {
};

// 异步函数
const actions = {

};

// 同步函数
const mutations = {
  setStart (state,t){
    state.isTemplate = t;
  },
  add_number_info (state,info){

    let extents = function (obj,length){
      let arr = new Array();
      for(let i=0;i<obj.length;i++){
        arr.push(
          {
            number: obj[i],
            start: 0
          }
        )
      }
      var max = length-obj.length;
      for(var i=0;i<max;i++){
        arr.push({
          number: '',
          start: -1
        })
      }
      return arr;
    };
    let coutArr = new Array()
    let p_onwArr = new Array()
    let p_towArr = new Array();
    let p1 = info.split('+')[0].split('');
    let p2 = info.split('+')[1].split('');
    let Result =  parseInt(info.split('+')[0])+parseInt(info.split('+')[1]);
    let count = Result.toString().split('')
    coutArr = extents(count,count.length);
    p_onwArr = extents(p1,count.length);
    p_towArr = extents(p2,count.length);

    let arr = [
      p_onwArr,
      p_towArr,
      coutArr
    ];

    state.set_number_info = arr;
  },
  add_active (state,obj){

    state.set_number_info[obj[0]][obj[1]].start=1;


  }
};

export default {
  state,
  getters,
  actions,
  mutations
}
